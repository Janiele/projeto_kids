package com.example.alunos.kinds;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Main3Activity extends AppCompatActivity {
    Button btnA3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        btnA3 = (Button)findViewById(R.id.button3);
    }
    public void btnA3(View v){
        Intent i = new Intent(this, Main4Activity.class);
        startActivity(i);
        finish();
    }
}
