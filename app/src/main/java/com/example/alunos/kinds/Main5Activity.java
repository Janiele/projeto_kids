package com.example.alunos.kinds;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

public class Main5Activity extends AppCompatActivity {
    TextView nome1;
    TextView nome2;
    Button btnA5;
    String valorAct1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main5);

        nome1 = (TextView)findViewById(R.id.textView12);
        nome2 = (TextView)findViewById(R.id.textView14);
        btnA5 = (Button)findViewById(R.id.button4);

        valorAct1 = getIntent().getExtras().getString("Value");
        nome1.setText(valorAct1);


    }
}
