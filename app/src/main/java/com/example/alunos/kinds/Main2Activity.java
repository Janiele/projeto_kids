package com.example.alunos.kinds;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {
    Button btnAvancar2;
    TextView nome1;
    String valorAct1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        btnAvancar2= (Button)findViewById(R.id.button2);
        nome1 = (TextView)findViewById(R.id.textView4);

        valorAct1 = getIntent().getExtras().getString("Value");
        nome1.setText(valorAct1);

    }
    public void btnAvancar2(View v){
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
        finish();

    }
}
