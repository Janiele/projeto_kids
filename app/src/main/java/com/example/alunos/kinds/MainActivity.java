package com.example.alunos.kinds;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    Button btnCriar;
    public EditText priNome;
    public EditText segNome;
    String edtValor1;
    String edValor2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        priNome = (EditText)findViewById(R.id.editText);
        segNome = (EditText)findViewById(R.id.editText2);
        btnCriar= (Button)findViewById(R.id.button);

    }
    public void  btnCriar(View v){

        Intent i = new Intent(this, Main2Activity.class);
        edtValor1 =  priNome.getText().toString();
        i.putExtra("Value", edtValor1);

        edValor2 = segNome.getText().toString();
        i.putExtra("Value",edValor2);
        finish();

    }


}
